'use strict';

const e = React.createElement;

class Bombe extends React.Component {
  constructor(props) {
    super(props);
    this.state = { minutes:3,secondes:59,explosion:false};
  }

  render() {
    return (
      <div>
     <h2>Tic Tac ....</h2>
     <input disabled type="texte" value={this.state.minutes+":"+this.state.secondes}/>
      </div>
    );
  }
}

const domContainer = document.querySelector('#root');
ReactDOM.render(<Bombe/>, domContainer);